import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="minizinc",
    version="0.1.1",
    author="Jip J. Dekker",
    author_email="<jjp@dekker.one>",
    description="Access MiniZinc directly from Python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/minizinc/minizinc-python",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Scientific/Engineering :: Mathematics",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy"
    ],
    python_requires='>=3.6',
)
